---
slug: "/blog/modifying-consoles"
date: "2021-08-02"
title: "modifying consoles"
---

I recently tried to modify an Xbox One S to make the fan quieter, and failed miserably. It still works, but the case was so hard to break off that the button to switch the console off no longer works, and that function is only usable via the controller. 

It's obvious that that particular machine isn't meant to be opened up, and I feel really naïve for believing the guides (such as they were) online asserting how "easy" the modification was. At least I'm telling myself I was a victim here, because the alternative is accepting that I'm just no good at these things.

What _supports_ my theory is that I've built several PCs (admittedly: painfully at times), modified a GameCube successfully to replace a laser assembly and the thermal pads, and put an ODE in my Saturn.

The er, _problem_ with my theory is that even more recently than the Xbox One S debacle, I managed to mess up adding a new fan to my old original PS2, and now it won't turn on. This is sad for me.

Maybe I just need practice, but it isn't exactly easy to get that with this kind of thing... 